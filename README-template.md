# Project Name


**Author:** Your name\
**email:** you@somewhere.edu  (preferably a persistent email address)\
**website:** where?\
Copyright YOUR NAME 2019 (or whatever license you want)


>>>
Introduce your code here. Briefly describe the physical model that it solves and the method used.
- What is your code **for**?
- Why would someone use it?
- Links to more extensive documentation elsewhere.

Block quotes (like this one) explain what each section is for. Most sections have ordinary text examples of what the section could include. 
>>>

## Quick Start

> Instructions to quickly get started with your program, e.g. how to compile and install with default settings and how to run it (it’s useful to include an easy way to test if they compiled and installed it correctly.  

To compile:

```
g++ -O3 main.cpp mtrand.cpp config.cpp
```

To run with `$NREP` replicas: 

```
mpirun -np $NREP ./a.out 
```


## Prerequisites

>>>
List the prerequisites that your code requires. These will be things like libraries and other software on your machine. Examples of dependencies include:
- Libraries (especially anything you had to install, like MPI)
- Anything platform-dependent
- A specific compiler required
- Other external programs
- uni10
>>>


## Program Files

> List the program files and what they are for. 

`main.cpp` --- Calls routines to run Monte Carlo and coordinates MPI communication between replicas, also reads and writes to files.\
`config.cpp` --- Main class that stores the QMC configuration and does the actual MC updates and measurements. \
`read.in` --- Options for almost all possible user-defined behavior, described later. \
`seed.in` --- **Optional** random seed input file. If not present program will take random seed from system clock. Should be automatically updated when the program finishes. **Note:** may not be properly updated if program does not exit normally. 


## Inputs

> Describe the input files (or command-line arguments) that your program requires. Make sure the user knows how to format the inputs, which parameters are optional, and what the defaults are. Make sure to define units, normalization, etc. 

The main user input file is `read.in`. The numbers in the leftmost column are the only thing imported by the program, the remaining columns are just a guide for users. Options should always be provided in this order. 

```
32		nsites		number of spins/sites
16		beta		max inverse temp
10		nbins		number of bins
10000	isteps		number of equilibration steps
5000	mcsteps		number of Monte Carlo steps per bin
1.0		HJ		Value of J in the Heisenberg model (positive for AF)
```

`nsites` -- Integer\
 Can take any even value 
 
`beta` -- Real\
Inverse temperature 

`nbins` -- Integer\
Number of MC bins to be preformed. \
Suggested value: >10

`isteps` -- Integer\
Number of initial equilibration Monte Carlo sweeps to be performed before beginning measurements. With beta doubling enabled this will be the length of each beta-doubling step. \
Suggested value: 10,000

`mcsteps` -- Integer\
Number of Monte Carlo sweeps to be performed in each bin. \
Suggested value: 10,000 

`HJ` -- Real\
Value of Heisenberg exchange coupling $J$. Positive for antiferromagnet, **negative (ferromagnetic) values will not work**. 



## Outputs

> Describe how the program outputs its results. Are the printed to stdout? Are they displayed on screen? Written out to file? What format is the output in? 
> 
> Describe the format of the data in each of the output files so a user can use the outputs without looking at your code to figure out what they are. Make sure to adequately describe any choices about normalization, units, etc. 

This program produces a number of output files. All data files end in `.txt` and are formatted as simple tab-separated rectangular arrays for easy importation into a post processing program. 

The first column of every output file is the magnetic field followed by one or more measured quantities. *There are no labels.* Each line represents a single bin. 

```
h	Q1	Q2
h	Q1	Q2
...
```

Below I will describe what will appear on each line of the output files. 

```
enrg.txt 
--> energy 
--> 2 columns
h_i		E_i

amag.txt 
--> uniform magnetization 
--> 3 col
h_i		<m_i>		<m^2_i>	

szIm.txt
--> non-equal time Sz correlations C^z(tau,r)
--> m*L+1 col
--> Normalized so C(0,0)=1
h_i		C(0,0)	C(0,1)	...	C(0,L-1)	C(1,0)	C(1,1)	...	C(m-1,L-1)

```


## References

>Provide references for the techniques used and the theory behind them.

[10 minute markdown tutorial](https://commonmark.org/)\
[Markdown quick reference guide](https://commonmark.org/help/)\
[Another guide to making README files](https://www.makeareadme.com/)


## Known Issues

>Describe any known problems with your code. There may be issues that don't affect your results, but could still trip up future users. 

