# computational-physics-readme-template

**Author:** Adam Iaizzi\
**email:** iaizzi@bu.edu\
**website:** <https://www.iaizzi.me>\
Copyright Adam Iaizzi 2019

This is a readme template that I built based on the needs of computational physics project to accompany this [blog post](https://iaizzi.me/04/25/write-a-good-readme). 

The template and explanatory text are in [README-template.md](README-template.md)


## Links

Blog post: https://iaizzi.me/04/25/write-a-good-readme

